# pipeline cleaner

> A Python script to remove failed and cancelled pipelines for the specified GitLab project. 

## Why?
I rely on GitLab CI for many projects. Once I have fixed a problem, I do not like seeing failed/canceled/skipped pipelines in the **All** tab of the **Pipelines** page in the GitLab UI. This project contains the Python I quickly wrote in order to:

  - solve the problem described above
  - give me an excuse to learn the GitLab API 

## Usage

  1. Generate a Personal Access Token
  2. Export that Personal Access Token to the environment variable `$GITLAB_PERSONAL_ACCESS_TOKEN`
  3. Specify the project you wish to tidy up:  `python pipeline-cleaner.py carranza-xyz`
  4. You will see a status report of the pipelines that are available to be cleaned up. You can ask the pipeline cleaner to remove those pipelines or provide you with a list of the pipelines that it found or just exit.

### GitLab CI
If you are running `pipeline-cleaner` in GitLab CI, make `$GITLAB_PERSONAL_ACCESS_TOKEN` a **Masked** custom environment variable. 

You can use the [.gitlab-ci.yml](.gitlab-ci.yml) file  in this repository.


### CLI
If you are running this elsewhere, set `GITLAB_PERSONAL_ACCESS_TOKEN` and then export that variable, like so:

```
$ GITLAB_PERSONAL_ACCESS_TOKEN=abcd1234
$ echo $GITLAB_PERSONAL_ACCESS_TOKEN
abcd1234
$ export $GITLAB_PERSONAL_ACCESS_TOKEN
```

With the variable set, use commands like these to clean up the pipelines for a project called `carranza-xyz`:

**Automatically remove failed pipelines**

```
python pipeline-cleaner.py  carranza-xyz auto
```

**Prompt to ask user about removing failed pipelines**

```
python pipeline-cleaner.py  carranza-xyz prompt
```


## Wish List

  - Tell the Pipeline Cleaner which statuses you wish to clean up (currently it assumes we have the same preferences)
  - Handle pagination properly
  - Make a report
  - Accept a list of projects to keep tidy (just run it in a `for` loop for now)
